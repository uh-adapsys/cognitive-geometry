from utility.display import plot_graph
from env.neighbourhood import Manhattan, Moore
from utility.exceptions import GridOutOfBounds, GoalSpecificationError, WorseThanRandomError

import numpy as np
import pandas as pd
import networkx as nx
from copy import deepcopy


class GridRoom:
    def __init__(self, options=None):
        """
        Create a room with discrete states located on a grid, based on the shape of the underlying world, specified
        as a tuple (max_y, max_x).   States are referenced by the integer index of the grid, all states are accessible
        unless they are specified as wall states.  If the goal(s) is not specified, then it is assigned to the first
        available state, i.e. not a wall state.

        There are several options, including the shape, Manhattan / Moore neighbourhood, determinism, ... which
        are passed in a dictionary of options. If no value is provided, a default is used.
            self.val2 = options.get('val2',"default value")

        :param options: dictionary of parameters according to the keywords:
            shape: tuple of 2 integers representing the grid size (max_y, max_x)
            walls: list of state integer indices for states which are walls, ignores states out of range
            mines: list of state integer indices for states which are mines, ignores states out of range
            goals: list of state integer indices for states which are goals
            mine_penalty: reward for entering a mine states, check 2 orders of magnitude greater than max value of any
            other state to ensure it acts as a deterrent - default value of 2e5
            manhattan: True or False
            determinism: 1 for 100% determinism, 0.97 for 3% apportioned between remaining actions, minimum
            acceptable value is uniform action distribution or greater else raises WorseThanRandomError
        """
        if options is None:
            options = {}
        self.shape = options.get('shape', (5, 5))
        self.nS = np.product(self.shape)
        self.MAX_Y, self.MAX_X = self.shape
        self.states = list(range(self.nS))
        self.goals = self.check_goals(options.get('goals', None))
        self.n_transient_S = len(self.states) - len(self.goals)
        # can the agent randomly start in the goal? n_transient_S or len(self.states)
        self.walls = list(filter(lambda wall: wall < np.product(self.shape), options.get('walls', [])))
        for s in set(self.walls):
            self.states.remove(s)
        self.isd = self.build_initial_state_distribution(len(self.states))

        self.mines = list(filter(lambda wall: wall < np.product(self.shape), options.get('mines', [])))
        self.mine_penalty = options.get('mine_penalty', -2e5)
        self.manhattan = options.get('manhattan', True)
        if self.manhattan:
            self.actions = Manhattan(self)
        else:
            self.actions = Moore(self)
        self.nA = self.actions.nA
        self.stochastic = False
        self.determinism = options.get('determinism', 1.0)
        if self.determinism > 1 or self.determinism < 1/self.nA:
            raise WorseThanRandomError
        if self.determinism != 1.0:
            self.stochastic = True

        self.P = self.build_transition_model()
        self.P_orig = deepcopy(self.P)
        self.D = self.update_dynamics_for_goals(self.goals)
        self.T, self.R = self.build_dynamics_as_array()
        self.G = self.build_graph()
        self.A = nx.adjacency_matrix(self.G)

        # twisted world
        self.epsilon = 0
        self.sigma = np.asarray([list(range(self.nA)) * self.nS]).reshape(self.nS, self.nA)

    def check_goals(self, goals):
        if not goals or goals is None:
            goals = [np.random.choice(self.states)]
        else:
            # check all the goals specified are valid states
            if not all(elem in self.states for elem in goals):
                raise GoalSpecificationError
        return goals

    def get_multi_index(self, s):
        """
        given the state and and knowledge of the shape, return zero based grid coordinates
        defensive programming - raise error if s out of bounds of np.prod(shape)

        :param s: integer representing zero based state index
        :return: y, x: (row, column) zero based grid coordinates
        """
        if s < 0 or np.prod(self.shape) <= s:
            raise GridOutOfBounds("state outside grid bounds: s {} shape: {}".format(s, self.shape))
        y, x = divmod(s, self.MAX_X)
        return y, x

    def get_state_sequence(self, s, g, policy, max_iters=1_000):
        """
        given a sample a single trajectory from the policy
        be aware that for larger gridworlds max_iters may need to be increased

        :param s: integer representing zero based state index
            g: integer representing zero based index of the goal state
            policy: behaviour policy as a numpy.ndarray 
        :return sequence: sampled trajectory of the agent following the policy as a list of state indices
        """
        sequence = [s]
        step_count = 0
        while step_count < max_iters:
            if s == g:   # agent starting state may be the goal
                break
            step_count += 1
            a = int(np.random.choice(self.nA, 1, p=policy[s].flatten()))
            # allow for multiple successor states
            ss_states = []
            ss_dist = []
            for p_ss_gs, ss, r, done in self.P[s][a]:
                ss_states.append(ss)
                ss_dist.append(p_ss_gs)
            # sample the distribution of successor states
            sequence.append(int(np.random.choice(ss_states, 1, p=ss_dist)))
            # print(step_count, s, policy[s].flatten(), a, env.P[s][a], sequence[-1], g)
            s = sequence[-1]
        return sequence   


    def is_wall(self, s):
        return s in self.walls

    def is_goal(self, s):
        return s in self.goals

    def is_mine(self, s):
        return s in self.mines

    def build_initial_state_distribution(self, n_legal_S):
        isd = np.ones(np.prod(self.shape)) / n_legal_S
        isd[self.walls] = 0
        return isd

    def build_transition_model(self):
        """
        Following the convention used in OpenAI Gym, the dynamics are stored in a dictionary of dictionaries,
        P[s][a] is used.  In this implementation, D[s][a] is used to distinguish between the transition
        matrix (nS x nS) used to calculate the state distribution and the combined model of the transition
        dynamics and the reward function.
            P[s][a] dictionary of dictionaries.
            P[S] = {a:[(p(ss|a,s), ss, R(ss|a,s), ss is_terminal), (p(ss'|a,s), ss', R(ss'|a,s), ss' is_term)], a2...}
        - one dictionary entry for each legal state
        - able to cope with it being stochastic... i.e. multiple successor states for an action/state pair
        - sum all P(*|a,s) = 1 * means for all successors states for each current state
        :return: P dynamics nested dictionaries - no goal information
        """
        def get_reward(state, successor_state):
            """
            helper function to return reward for transition from state to successor_state
            :param state: initial state before transition
            :param successor_state: state after transition
            :return: reward for transition R(s'|s, a)
            """
            result = -1  # all transient states apart from mine states receive a penalty of -1
            if self.is_mine(successor_state):
                result = self.mine_penalty
            return result

        P = {}
        p_ss_g_as = self.determinism
        p_ss_wind_g_as = np.round((1 - p_ss_g_as) / (self.nA - 1), 6)
        grid = np.arange(int(np.product(self.shape)))
        states = np.nditer(grid, flags=['multi_index'])
        while not states.finished:
            s = states.iterindex
            P[s] = {a: [] for a in range(self.nA)}
            # transient state
            for a in range(self.nA):
                ss = self.actions.get_successor_state(s, a)
                if self.is_wall(ss):
                    ss = s
                P[s][a].append((p_ss_g_as, ss, get_reward(s, ss), False))
                if self.stochastic:
                    for a_wind in range(self.nA):
                        if a_wind != a:
                            s_wind = self.actions.get_successor_state(s, a_wind)
                            if self.is_wall(s_wind):
                                s_wind = s
                            P[s][a].append((p_ss_wind_g_as, s_wind, get_reward(s, s_wind), False))
            states.iternext()
        return P

    def update_dynamics_for_goals(self, goals):
        """
        Create a copy of the transition model and modify for
        - goal successor states are terminal and all future actions have zero reward and do not move

        :param: list of goal states
        :return: D dynamics nested dictionaries
        """
        self.goals = goals
        D = deepcopy(self.P)
        for goal in self.goals:
            for neighbour in self.actions.get_neighbour_states(goal):
                for a in range(self.nA):
                    for count, transition in enumerate(D[neighbour][a]):
                        p_ss_G_a_s, ss, _, _ = transition
                        if ss == goal:
                            D[neighbour][a][count] = (p_ss_G_a_s, goal, -1, True)
            for a in range(self.nA):
                for count, transition in enumerate(D[goal][a]):
                    p_ss_G_a_s, _, _, _ = transition
                    D[goal][a][count] = (p_ss_G_a_s, goal, 0, True)

        self.D = D
        self.T, self.R = self.build_dynamics_as_array()
        self.G = self.build_graph()
        return D

    def build_dynamics_as_array(self):
        T = np.zeros((self.nS * self.nA, self.nS))
        R = np.zeros((self.nS * self.nA, self.nS))
        row_labels = []

        for s in self.D.keys():
            for a in self.D[s].keys():
                row_labels.append('{},{}'.format(s, a))
                for p, ss, r, _ in self.D[s][a]:
                    T[s * self.nA + a, ss] += p
                    R[s * self.nA + a, ss] = r
        return pd.DataFrame(T, columns=np.arange(self.nS), index=row_labels), \
            pd.DataFrame(R, columns=np.arange(self.nS), index=row_labels)

    def build_graph(self):
        G = nx.Graph()
        edges = set()
        for node in self.D.keys():
            if self.is_goal(node):
                color = 'y'
            elif self.is_mine(node):
                color = 'r'
            else:
                color = 'orange'
            G.add_node(node, pos=self.get_multi_index(node), color=color)
            for action in self.D[node].keys():
                for _, ss, _, _ in self.D[node][action]:
                    if ss is node:
                        pass
                    else:
                        edges.add((node, ss))
        G.add_edges_from(list(edges), length=1)
        return G

    def get_dynamics(self):
        return self.D

    def plot_graph(self, filename=None):
        fig = plot_graph(graph=self.G, shape=self.shape, label=True, filename=filename)

    def build_sigma(self, epsilon):
        """
        Build the permutation function in the form of a 2D array (nS, nA).  Starting with untwisted sigma:
        np.asarray([list(range(self.nA)) * self.nS]).reshape(self.nS, self.nA), consider each state, randomly choose
        a float in the half-open interval [0.0, 1.0).  If less than or equal to epsilon, shuffle action labels.
        :param epsilon: probability of a twisted state
        :return: nothing, sigma is stored
        """
        self.epsilon = epsilon
        self.sigma.flags.writeable = True
        for i, row in enumerate(self.sigma[:]):
            if np.random.random() <= epsilon:
                np.random.shuffle(row)
                # print("*", end='')
            # print(i, row)

    def twist_dynamics(self):
        P_twisted = {}
        for s in self.P.keys():
            P_twisted[s] = {a: [] for a in range(self.nA)}
            for a in range(self.nA):
                P_twisted[s][self.sigma[s, a]] = self.P[s][a]
        self.P = P_twisted

    def twist_world(self, epsilon):
        self.epsilon = epsilon
        self.build_sigma(epsilon)
        self.twist_dynamics()
        self.update_dynamics_for_goals(self.goals)
        self.T, self.R = self.build_dynamics_as_array()

    def __str__(self):
        """
        Render the current grid world environment as a string
        :return: string representation of the grid world
        """
        grid = np.arange(int(np.prod(self.shape))).reshape(self.shape)
        states = np.nditer(grid, flags=['multi_index'])
        output = []
        while not states.finished:
            s = states.iterindex
            y, x = states.multi_index

            if s in self.goals:
                string = " G "
            elif s in self.mines:
                string = " M "
            elif s in self.walls:
                string = " + "
            else:
                string = " o "

            if x == 0:
                string = string.lstrip()
            if x == self.shape[1] - 1:
                string = string.rstrip()

            output.append(string)

            if x == self.shape[1] - 1:
                output.append("\n")
            states.iternext()
        return ''.join(output)


def main():
    import pprint as pp
    p = pp.PrettyPrinter()
    # options = {'shape': (5, 5), 'goals': [4, 8], 'manhattan': False, 'determinism': 0.85}
    options = {'shape': (2, 2), 'goals': [0], 'determinism': 1}
    grid = GridRoom(options)
    print("\ngrid.P:\n")
    p.pprint(grid.P)
    print("\ngrid.D\n")
    p.pprint(grid.D)
    grid.plot_graph()
    T, R = grid.build_dynamics_as_array()
    print(f"grid wolrd dynamics:\n{T}")
    # sample a trajectory from arbitrary states to the goal
    states = np.random.choice(grid.nS, 5, p=grid.isd)
    g = grid.goals[0]
    print(f"\nSampled trajectories from the following states: {states} to the goal: {g}.")
    # create a uniform policy
    policy = np.ones((grid.nS, grid.nA)) / grid.nA
    for s in states:
        print(grid.get_state_sequence(s,g, policy, max_iters=10))




if __name__ == '__main__':
    main()
