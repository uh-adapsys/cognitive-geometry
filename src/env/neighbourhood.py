from utility.exceptions import GridOutOfBounds
from abc import ABC


class Neighbourhood(ABC):
    """
    Abstract class.  A neighbourhood defines the states accessible from a current state.  For example, it could be
    the Manhattan neighbourhood in which only the states on the 4 cardinal points are accessible.  Currently implemented
    in the context of navigating within a single room grid world with actions described by their direction.
    """
    def __init__(self, env):
        """
        Environment object env provides means to decode the integer index of a state to the multi-index coords. Child
        class defines details of the actions.

        :param env: accesses shape for MAX_Y and MAX_X and the get_multi_index method.
        """
        self.MAX_Y, self.MAX_X = env.shape
        self.get_multi_index = env.get_multi_index
        self.action_names = None
        self.action_functions = None
        self.nA = None

    def get_successor_state(self, s, a):
        """
        return the intended successor state for the s, a pair passed
        won't accept a state that is outside of the grid world.
        :param s: zero based state index (integer)
        :param a: zero based action index (integer)
        :return: ss: successor for the determinism case (integer)
        :raises: raises a GridOutOfBounds exception if the state is outside the max x and y values
        """

        if s < 0 or s >= self.MAX_Y * self.MAX_X <= s:
            raise GridOutOfBounds("state outside grid bounds: s {} MAX_Y: {} MAX_X: {}".
                                  format(s, self.MAX_Y, self.MAX_Y))
        action = self.action_functions[a]
        return action(s)

    def get_neighbour_states(self, s):
        """
        return a set of all neighbouring states given the Manhattan actions
        set ensures no duplication

        :param s: current state
        returns a set of integers representing zero based states
        """
        neighbours = set()
        for a in range(self.nA):
            neighbours.add(self.get_successor_state(s, a))
        return neighbours

    def get_nA(self):
        return self.nA


class Manhattan(Neighbourhood):
    def __init__(self, env):
        super(Manhattan, self).__init__(env)
        self.action_names = {'N': 0, 'E': 1, 'S': 2, 'W': 3}
        self.action_functions = {0: self.n, 1: self.e, 2: self.s, 3: self.w}
        self.nA = 4

    def n(self, s):
        y, x = self.get_multi_index(s)
        return s if y == 0 else s - self.MAX_X

    def e(self, s):
        y, x = self.get_multi_index(s)
        return s if x == (self.MAX_X - 1) else s + 1

    def s(self, s):
        y, x = self.get_multi_index(s)
        return s if y == (self.MAX_Y - 1) else s + self.MAX_X

    def w(self, s):
        y, x = self.get_multi_index(s)
        return s if x == 0 else s - 1


class Moore(Manhattan):
    def __init__(self, env):
        super(Moore, self).__init__(env)
        self.action_names = {'N': 0, 'NE': 1, 'E': 2, 'SE': 3, 'S': 4, 'SW': 5, 'W': 6, 'NW': 7}
        self.action_functions = {0: self.n, 1: self.ne, 2: self.e, 3: self.se,
                                 4: self.s, 5: self.sw, 6: self.w, 7: self.nw}
        self.nA = 8

    def ne(self, s):
        y, x = self.get_multi_index(s)
        if y == 0 or x == (self.MAX_X - 1):
            result = s
        else:
            result = self.e(self.n(s))
        return result

    def se(self, s):
        y, x = self.get_multi_index(s)
        if y == (self.MAX_Y - 1) or x == (self.MAX_X - 1):
            result = s
        else:
            result = self.e(self.s(s))
        return result

    def nw(self, s):
        y, x = self.get_multi_index(s)
        if y == 0 or x == 0:
            result = s
        else:
            result = self.w(self.n(s))
        return result

    def sw(self, s):
        y, x = self.get_multi_index(s)
        if y == (self.MAX_Y - 1) or x == 0:
            result = s
        else:
            result = self.w(self.s(s))
        return result
