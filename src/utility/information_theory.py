import numpy as np
import gmpy2 as mp

from scipy.special import xlogy


def get_entropy(p):
    """
    Return the expected entropy for a given vector of probabilities, for example
    in the case of a policy representing p(a|s), calculate E[H[p(a|s)]].
    The problem is the occurrence of zero probabilities.  An option is to mask
    the policy which efficiently ignores zero values.

    Alternatively, https://stackoverflow.com/a/21760908
    use combination of np.sign and sciy.special.xlogy which works on vectors.
    np.sign(p) returns -1 if p<0, 0 if p=0 and 1 if p>0

    Args:
        p nd array where p[s][a] = p(a|s) of shape(nS, nA)

    Returns:
        h(a|s)  the entropy for the available actions in the policy, given the state

    """

    return np.sum(-p * log2_ignore_zero(p))


def get_multi_index(s, shape):
    """
    Given the state and shape, returns zero based grid coordinates
    Args:
        s int     zero-based integer representing states
        shape  tuple  (y_max, x_max) of grid world
    """
    Y = shape[0]
    X = shape[1]
    y, x = divmod(s + 1, X)
    return y, x - 1


def log2_ignore_zero(p):
    """
    https://stackoverflow.com/a/21760908
    use combination of np.sign and sciy.special.xlogy which works on vectors.
    np.sign(p) returns -1 if p<0, 0 if p=0 and 1 if p>0
    probabilities should never be -ve, but if they are, it will return nan
    """
    return xlogy(np.sign(p), p) / np.log(2)


def mp_log2_ignore_zero(p):
    """
    np.sign(p) returns -1 if p<0, 0 if p=0 and 1 if p>0
    https://stackoverflow.com/questions/33059198/gmpy2-log2-not-accurate-after-16-digits
    """
    if not np.sign(p):
        result = mp.log(p)/mp.log(2)
    else:
        result = 0
    return result


def log10_ignore_zero(p):
    """
    https://stackoverflow.com/a/21760908
    use combination of np.sign and sciy.special.xlogy which works on vectors.
    np.sign(p) returns -1 if p<0, 0 if p=0 and 1 if p>0
    """
    return xlogy(np.sign(p), p) / np.log(10)


def div_ignore_zero(a, b):
    """
    ignore / 0, div0( [-1, 0, 1], 0 ) -> [0, 0, 0]
    https://stackoverflow.com/a/35696047
    """
    with np.errstate(divide='ignore', invalid='ignore'):
        try:
            c = np.true_divide(a, b)
            c[~ np.isfinite(c)] = 0  # -inf inf NaN
        except RuntimeWarning:
            print('Error 11 - runtime warning - testing for convergence')
            pass
    return c


def mp_div_ignore_zero(a, b):
    mp.get_context().trap_divzero = True
    try:
        c = mp.div(a, b)
    except mp.DivisionByZeroError:
        c = 0
    return c


def set_zeros(array, decimal=12):
    array[np.where(array < 1 / 10 ** 12)] = 0

def conditional_kullback_leibler_divergence(p, q, p_x):
    """
    D_KL[p(y|x)||q(y|x)] = sum over x p(x) sum over y p(y|x) log [p(y|x)/q(y|x)]

    Need to check for 0 log 0, 0/0, a/inf, a/-inf
    Args:
        p:
        q:
    """
    return np.sum(np.multiply(p_x, np.sum(np.multiply(p, log2_ignore_zero(div_ignore_zero(p, q))), axis=1)))


def get_mutual_information_by_state(policy, p_s):
    p0_a = policy.T @ p_s
    with np.errstate(divide='ignore', invalid='ignore'):
        return np.nansum(np.multiply(policy, log2_ignore_zero(np.divide(policy, p0_a))), axis=1)


def get_expected_utility_by_state(policy, utility):
    """
    given a state-action utility matrix, return a state utility matrix, e.g. given Q(s,a) return V(s)
    :param policy: np array including np.nan values for illegal states
    :param utility:
    :return:
    """
    np.sum(np.multiply(policy, utility), axis=1)


def get_expected_value(p_s, p_a_Gs, utility):
    """
    :param p_s: state distribution, e.g. env.get_isd()
    :param p_a_Gs: policy
    :param utility: state action utility array
    :return: expected heatmap_var given the probability distributions
    """
    return np.sum(np.multiply(p_s, np.sum(np.multiply(p_a_Gs, utility), axis=1)))


def get_expected_scalar(p_s, vector):
    # nansum treats nans as zero
    return np.nansum(np.multiply(p_s, vector))


def get_expected_vector(policy, matrix):
    # nansum treats nans as zero
    return np.sum(np.multiply(policy, matrix), axis=1)