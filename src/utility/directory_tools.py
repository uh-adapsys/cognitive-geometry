import os


def get_data_path(data_dir='data/', git_repo = 'cognitive-geometry'):
    path = os.path.abspath('')
    prefix = path[:path.find(git_repo)]
    return os.path.join(prefix, git_repo, data_dir)
