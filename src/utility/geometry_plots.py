from sklearn.manifold import MDS

import numpy as np
import networkx as nx

import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib import colors as mcolors
from matplotlib.text import Annotation
from mpl_toolkits.mplot3d.proj3d import proj_transform
from mpl_toolkits.mplot3d import Axes3D

# some default configuration values for the model
eps = 1e-5
max_iter = 300


def get_matrix_embedding(matrix, state=False, components=2):
    """
    Given a symmetric adjacency matrix, return the optimal embedding of the objects as either 2D or 3D coordinates.
    Uses multi-dimensional scaling (MDS) to calculate the optimum embedding. Each row/column in the matrix represents
    an object which is embedded in the space given by the component provided.

    @param matrix: adjacency matrix of objects to be embedded
    @param state: either supply a value or a random integer value will be used
                  to initialise the MDS iterative algorithm
    @param components: either 2 or 3 - defaults to 2
    @return: coordinate values of objects in the matrix supplied
    """
    if not state:
        state = np.random.randint(0, 1e4)
    # run sklearn MDS algorithm
    model = MDS(n_components=components, random_state=state, dissimilarity='precomputed',
                eps=eps, max_iter=max_iter)
    coords = model.fit_transform(matrix)

    return coords


def calculate_symmetric_adjacency_matrix(matrix):
    """
    Given a (square) adjacency matrix, return the symmetrised values, i.e. the average values given by
    d_ij^sym = (d_ij + d_ji)/2 where d_ij is the i,jth element of the matrix provided.

    @param matrix: square matrix of distances
    @return: symmetrised adjacency matrix
    """

    return (matrix + matrix.T) / 2


class Annotation3D(Annotation):
    """
    3D Annotation object located at point xyz, with text provided.
    """

    def __init__(self, string, xyz, *args, **kwargs):
        Annotation.__init__(self, string, xy=(0,0), *args, **kwargs)
        self._verts3d = xyz # tuple of (x,y,z)

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.xy = (xs, ys)
        Annotation.draw(self, renderer)


def annotate_3D(axes, string, *args, **kwargs):
    """
    Add annotation text to Axes3D provided.
    @param axes: Axes3D to be annotated
    @param string: text for the annotation
    @param args: any extra details provided, e.g. font, size to be passed onto artist
    @param kwargs: as above
    """
    tag = Annotation3D(string, *args, **kwargs)
    axes.add_artist(tag)


# function to build graph either using 2 or 3 coordinates
def build_embedded_graph(coords, env, node_colours, components=3):
    G = nx.Graph()
    # works for 2 or 3 coords
    if components == 2:
        coords = np.c_[coords, np.zeros(coords.shape[0])]
    edges = set()
    for node, coord in enumerate(coords):
        G.add_node(node, pos=tuple(coord), color=node_colours[node])
        for action in env.D[node].keys():
            for _, ss, _, _ in env.D[node][action]:
                if ss is node:
                    pass
                else:
                    edges.add((node, ss))
    G.add_edges_from(list(edges), length=2)
    return G

# https://www.idtools.com.au/3d-network-graphs-python-mplot3d-toolkit/
def network_plot_3D(G, angle=0, annotate=False, save=False):
    # Get node positions
    pos = nx.get_node_attributes(G, 'pos')
    node_colours = [*nx.get_node_attributes(G, 'color').values()]
    scale = 30

    # 3D network plot
    with plt.style.context('ggplot'):
        fig = plt.figure(figsize=(10, 7))
        ax = Axes3D(fig)
        # ax = fig.add_subplot(111, projection='3d')
        plt.axis('auto');

        # Loop on the pos dictionary to extract the x,y,z coordinates of each node
        for key, value in pos.items():
            xi = value[0]
            yi = value[1]
            zi = value[2]
            node_colour = node_colours[key]

            # Scatter plot
            ax.scatter(xi, yi, zi, c=node_colour, s=scale ** 2, edgecolors='gray', alpha=0.6, zorder=5)

        # Loop on the list of edges to get the x,y,z, coordinates of the connected nodes
        # Those two points are the extrema of the line to be plotted
        for i, j in enumerate(G.edges()):
            x = np.array((pos[j[0]][0], pos[j[1]][0]))
            y = np.array((pos[j[0]][1], pos[j[1]][1]))
            z = np.array((pos[j[0]][2], pos[j[1]][2]))

            # Plot the connecting lines
            ax.plot(x, y, z, c='gray', alpha=0.5)

    if annotate:
        shift = max(max(max(pos.values())) / 50, 1)
        shift = scale / 2.8
        for j, xyz_ in enumerate(pos.values()):
            annotate_3D(ax, string=str(j), xyz=xyz_, fontsize=12, xytext=(shift, -shift),
                       textcoords='offset points', ha='right', va='bottom', weight='bold', zorder=15)

            # Set the initial view
    ax.view_init(30, angle)
    # Hide the axes
    ax.set_axis_off()
    if save:
        plt.savefig("test" + str(angle).zfill(3) + ".png")
    return ax


