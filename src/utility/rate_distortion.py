import inspect
import numpy as np
import gmpy2 as mp


def policy_blahut_step(env, p_a_k, exponential):
    """
    Rate Distortion optimisation for communicating signal X -> Y
    using Lagrange Multiplier beta and distortion matrix D
    multi precision floating arithmetic may be required owing to exponential
        p_a_G_s_next = p_a_k * exp[-exponential].normalised
        p_a_k = the marginal distribution of the policy @ prior state distribution
            np.ma.masked_invalid(policy).filled(0).T @ self.p0_s
        p_a_G_s represents the policy, i.e. the conditional probability P(a|s)
        invalid states (walls) set to np.nan

    useful information about mpfr:
    https://stackoverflow.com/questions/26539163/elementwise-operations-in-mpmath-slow-compared-to-numpy-and-its-solution?lq=1
    https://www.programcreek.com/python/example/98526/gmpy2.mpz

    Params:
        p_a_k:  marginal action distribution based on current policy estimate and prior state distribution
        exponential:  Free energy for Information-to-go beta*D for Rate Distortion (nS, nA)

    Returns:
        next estimate of policy p_a_G_s shape (nS, nA) and Z, the partition function
    """
    p_a_k = np.tile(p_a_k, (exponential.shape[0], 1)).flatten()
    try:
        F = np.nan_to_num(exponential).flatten()
        exp_term = np.array([mp.exp2(-x) for x in F])
        result = np.array([mp.mul(x, y) for (x, y) in zip(exp_term, p_a_k)]).reshape(exponential.shape)
        Z = np.array([mp.fsum(row) for row in result])
        policy = np.array(
            [mp.div(single, total) for (single, total) in zip(result.flatten(), np.repeat(Z, env.nA))]).reshape(
            exponential.shape)
        policy = policy.astype(float)
        policy[np.argwhere(policy == 0)] += 1e-303
        policy[env.walls[:], :] = np.nan
        Z[env.walls] = np.nan
        return policy, Z

    except FloatingPointError as err:
        # use multiple precision
        print("Floating point error - even though using mpfr {}".format(inspect.currentframe()))
        print(mp.get_context())
        print("Error: {} ".format(err))
        exit(7)
