class GridOutOfBounds(ValueError):
    """
    This is raised when representing a state outside of the grid.
    s >= np.prod(shape) or s<0.
    Note neighbourhood doesn't care about illegal states, only the boundary of the grid.
    """
    pass


class WorseThanRandomError(ValueError):
    """
    determinism should be a float between 1 and 1/nA.  default value is 1
    """
    pass


class GoalSpecificationError(ValueError):
    """
    options for goal states passed to environment constructor contain errors
    typically means that goals are partially specified or goal is a wall state
    """
    pass


class PolicyNotKnownError(ValueError):
    """
    Raised when trying to generate a policy plot and the policy is None, i.e. not calculated yet.
    """
    pass


class ValueNotCalculated(ValueError):
    pass


class PolicyNotCalculated(ValueError):
    pass


class InformationNotCalculated(ValueError):
    pass


class FreeEnergyNotCalculated(ValueError):
    pass
