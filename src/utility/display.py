import networkx as nx

import numpy as np
import numpy.ma as ma

import pandas as pd

from matplotlib import pyplot as plt
from matplotlib import colors as mcolors
from matplotlib import ticker, colors
from matplotlib import patches
import matplotlib as mpl
from mpl_toolkits.axes_grid1 import make_axes_locatable

from utility.information_theory import get_expected_vector

import warnings
# networkx/drawing/nx_pylab.py:579: MatplotlibDeprecationWarning:
# The iterable function was deprecated in Matplotlib 3.1 and will be removed in 3.3. Use np.iterable instead.
#   if not cb.iterable(width):
warnings.filterwarnings("ignore", category=UserWarning)

# Set the font dictionaries (for plot title and axis titles)
title_font = {'fontname': ['Arial', 'DejaVu Sans'], 'size': '14', 'color': 'black', 'weight': 'bold',
              'verticalalignment': 'bottom'} # Bottom vertical alignment for more space
text_font = {'fontname': ['Arial', 'DejaVu Sans'], 'size': '14', 'color': 'black', 'weight': 'normal',
              'verticalalignment': 'bottom'}
axis_font = {'fontname': ['Arial', 'DejaVu Sans'], 'size': '14', 'weight': 'normal'}
label_font = {'fontname': ['Arial', 'DejaVu Sans'],  'size': '16', 'color': 'black', 'weight': 'normal',
              'verticalalignment': 'top'}  # change the size and spacing in the add_labels_to_plot arguments
light_title_font = {'fontname': ['Arial', 'DejaVu Sans'], 'size': '16', 'color': 'black', 'weight': 'normal',
              'verticalalignment': 'bottom', 'horizontalalignment': 'left'} # Bottom vertical alignment for more space


DEFAULT_CMAP = 'Oranges'  # set the default color map - for colorblind compatibility use cividis or single hue


# graph plot cmap - maps color_indices to RGB colors
GRAPH_CMAP = mpl.colors.ListedColormap([
    mcolors.cnames['firebrick'],      # 0 outer corners
    mcolors.cnames['lightcoral'],     # 1 outer edge
    mcolors.cnames['darkorange'],     # 2 2nd corner
    mcolors.cnames['orange'],         # 3 edge
    mcolors.cnames['darkgoldenrod'],  # 4 3th corner
    mcolors.cnames['gold'],           # 5 edge
    mcolors.cnames['steelblue'],      # 6 4th corner
    mcolors.cnames['cornflowerblue'], # 7 edge
    mcolors.cnames['darkviolet'],     # 8 5th corner
    mcolors.cnames['blueviolet'],     # 9 edge
    mcolors.cnames['crimson']         # 10 centre
])


def format_number(number):
    """
    Given a float, returns a string representation suitable for or display on plot and in filename
    Args:
        number:
    Returns:
        string representation of float
        for zero? np.round(it[0], 2)
    """

    if np.isclose(number, 0):
        fmt = "0"
    elif abs(number) < 1e-3:
        fmt = "{:1.1g}"
    elif abs(number) < 0.1:
        fmt = "{:4.3f}"
    else:
        fmt = '{:.3g}'
    return fmt.format(number)


def get_figsize(shape, height=8):
    """
    Get a consistent figsize for all plots, using aspect ratio of grid world shape
    Args:
        shape: gridworld dimensions
        height: intended height of plot
    Returns:
        figsize for matplotlib plots in inches
    """
    width = (shape[0]/shape[1]) * height
    return height, width


def plot_graph(graph, shape, node_colours=None, height=7, filename=None, label=None, cmap=DEFAULT_CMAP):
    # need to fix aspect ratio... fig, ax = plt.subplots()
    fig, ax = plt.subplots(figsize=get_figsize(shape, height))
    ax.axis('off')
    fig.tight_layout()
    node_size = 20000 / (shape[0]*3)
    font_size = 120 / shape[0]
    if not node_colours:
        node_colours = [*nx.get_node_attributes(graph, 'color').values()]
    flipped_pos = {node: (y, -x) for (node, (x, y)) in nx.get_node_attributes(graph, 'pos').items()}
    nx.draw_networkx(graph, flipped_pos, node_color=node_colours, with_labels=label, node_size=node_size,
                     font_size=font_size, alpha=0.7, edge_color="gray", linewidths=2)
    plt.margins(0.2)

    if filename is not None:
        fig.savefig(filename, bbox_inches='tight', pad_inches=0.1)
    return plt


def plot_sequence(shape, G, path, label=True, filename=None):
    node_colours = [*nx.get_node_attributes(G, 'color').values()]
    seq_colors = [mcolors.cnames['lawngreen'] if n in path else node_colours[n] for n in G.nodes()]
    return plot_graph(G, shape, node_colours=seq_colors, label=True, filename=filename)


def add_label_to_plot(ax, matrix, shift=0., size=14):
    # add label to each cell
    it = np.nditer(matrix, flags=['multi_index'])
    labels = []
    # adjust size for grid sizes > 7
    if matrix.shape[0] > 7:
        size = 12
    label_font['size'] = size
    while not it.finished:
        labels.append(ax.text(it.multi_index[1] + shift, it.multi_index[0] + 0.6 * shift, format_number(it[0]),
                              ha="center", va="center", **label_font))
        it.iternext()
    return labels


def add_rectangle_to_grid_for_goals(ax, env):
    # add a rectangle to highlight the goal state
    # goal state stored as single index, need to convert to multi-index
    for goal in env.goals:
        gy, gx = env.get_multi_index(goal)
        # rect = patches.Rectangle((gx+0.1, gy+0.1), 0.8, 0.8, linewidth=5, edgecolor='g', facecolor='g', alpha=0.7)
        rect = patches.Rectangle((gx+0.025, gy+0.025), 0.95, 0.95, linewidth=5, edgecolor='y', fc=(1,1,0,0.3))
        ax.add_patch(rect)


def add_rectangle_to_grid_for_mines(ax, env):
    # add a rectangle to highlight the goal state
    # goal state stored as single index, need to convert to multi-index
    for mine in env.mines:
        gy, gx = env.get_multi_index(mine)
        rect = patches.Rectangle((gx-0.5, gy-0.5), 1, 1, linewidth=5, edgecolor='r', facecolor='r', alpha=0.5)
        rect = patches.Rectangle((gx+0.1, gy+0.1), 0.8, 0.8, linewidth=5, edgecolor='r', facecolor='r', alpha=0.5)
        ax.add_patch(rect)


def add_rectangle_to_grid_for_walls(ax, env):
    # add a rectangle to highlight the goal state
    # goal state stored as single index, need to convert to multi-index
    for wall in env.walls:
        gy, gx = env.get_multi_index(wall)
        rect = patches.Rectangle((gx-0.5, gy-0.5), 1, 1, linewidth=5, edgecolor='k', facecolor='k', alpha=0.7)
        rect = patches.Rectangle((gx+0.1, gy+0.1), 0.8, 0.8, linewidth=5, edgecolor='k', facecolor='k', alpha=0.7)
        ax.add_patch(rect)


def save_plot(fig, filename):
    if fig is not None:
        plt.savefig(filename,
                    bbox_inches='tight',
                    pad_inches=0)


def plot_quiver(env, policy, height=7, policy_label=" ", ax=None, filename=None, beta=None, zoom=False, auto=False, title=False):
    """
    :param policy: Policy object which can reference env and values
    :param height:
    :param policy_label:
    :param filename:
    :param beta: for labelling
    :param auto: not really implemented, but idea is to be able to set zoom.
    :return: ax  returns the axes of the plot
    """
    shape = env.shape
    if ax is None:
        fig, ax = plt.subplots(figsize=get_figsize(shape, height))

    ax.invert_yaxis()
    ax.grid(b=True, which='major', color='#666666', linestyle='-')
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('none')
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_ylim(shape[0], 0)
    ax.set_xlim(0, shape[1])

    # TODO replace with quiver function in neighbourhood
    if env.actions.nA == 4:
        N = 0
        E = 1
        S = 2
        W = 3
    else:
        N = 0
        NE = 1
        E = 2
        SE = 3
        S = 4
        SW = 5
        W = 6
        NW = 7

    df = pd.DataFrame(policy)
    Y, X = np.divmod(np.arange(env.nS), env.shape[1])
    Y = Y + 0.5
    X = X + 0.5
    zeros = np.zeros(len(Y))
    scale = 2
    if auto:
        if np.amax(df.max(axis=0)) <= 0.4:
            zoom = True
    if zoom:
        scale = 1

    kw = {'units': 'xy', 'scale_units': 'xy', 'scale': scale, 'width': 0.025, 'angles':  'xy', 'headwidth': 2,
          'headlength': 3}

    ax.quiver(X, Y, df[E].values, zeros, **kw)
    ax.quiver(X, Y, -df[W].values, zeros, **kw)
    ax.quiver(X, Y, zeros, -df[N].values, **kw)
    ax.quiver(X, Y, zeros, df[S].values, **kw)

    if env.actions.nA > 4:
        ax.quiver(X, Y, df[NE].values, -df[NE].values, **kw)
        ax.quiver(X, Y, df[SE].values, df[SE].values, **kw)
        ax.quiver(X, Y, -df[SW].values, df[SW].values, **kw)
        ax.quiver(X, Y, -df[NW].values, -df[NW].values, **kw)

    if title:
        if beta:
            options = "[beta={:1.2E}]".format(beta)
        else:
            options = ""
        ax.set_title(r"Policy Plot $\pi^{}(s,a)$ {}".format(policy_label, options), y=1.08, **title_font)

    if zoom:
        plt.text(0.05, -0.025, "policy arrow zoom: {}".format("200%" if zoom else "100%"),
                 transform=ax.transAxes, horizontalalignment='left',  **label_font)

    if filename is not None:
        fig.savefig(filename, bbox_inches='tight', pad_inches=0.1)
    return ax


def plot_heatmap(env, heatmap_var, title=None, height=7, filename=None, clim=None, label=False, label_state=False):
    # TODO fix all calls to this function now that shape removed as arg
    fig, ax = plt.subplots(figsize=get_figsize(env.shape, height))

    fig.tight_layout()
    plt.margins(0.05)
    # im = ax.imshow(heatmap_var, cmap=plt.get_cmap('GnBu'))
    im = heatmap_imshow(env, heatmap_var, ax)
    if clim is not None:
        im.set_clim(clim)

    # create colour bar that aligns height with axes
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="3%", pad=0.05)
    ax.figure.colorbar(im, cax=cax)

    ax.invert_yaxis()
    ax.grid(b=True, which='major', color='#666666', linestyle='-')
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('none')
    ax.set_xticklabels([])
    ax.set_yticklabels([])
    ax.set_ylim(env.shape[0], 0)
    ax.set_xlim(0, env.shape[1])

    # add label to each cell
    if label:
        if label_state:
            add_label_to_plot(ax, np.arange(env.nS).reshape(env.shape), 0.4, size=16)
        else:
            add_label_to_plot(ax, heatmap_var.reshape(env.shape), 0.25)
    if title:
        ax.set_title(title, x = 0.1, y=1.08, **title_font)
    if filename is not None:
        fig.savefig(filename, bbox_inches='tight', pad_inches=0.1)
    return ax


def plot_contour(env, contour_var, title=None, height=7, label=True):
    fig, ax = plt.subplots(figsize=get_figsize(env.shape, height))
    # configure tick labels for states
    Y = np.arange(env.shape[0] + 1)
    X = np.arange(env.shape[1] + 1)
    # add ticks to middle of cell
    ax.xaxis.set_major_locator(plt.MaxNLocator(env.shape[1]))
    ax.yaxis.set_major_locator(plt.MaxNLocator(env.shape[0]))
    ax.set_xticklabels(X-1)
    ax.set_yticklabels(Y-1)
    fig.tight_layout()
    ax.invert_yaxis()
    # do not add ticks
    ax.xaxis.set_ticks_position('none')
    ax.yaxis.set_ticks_position('none')
    ax.set_xticklabels([])
    ax.set_yticklabels([])

    # lev_exp = np.arange(np.floor(it.log10_ignore_zero(Z.min())-1), np.ceil(np.log10(Z.max())+1))
    # levels = np.power(10, lev_exp)
    im = ax.imshow(contour_var, cmap=DEFAULT_CMAP, origin='upper')
    contours = plt.contour(contour_var, env.nS, colors='Gray',  extent=[-0.5, env.shape[1]-0.5, -0.5, env.shape[0]-0.5])

    fmt = ticker.LogFormatterMathtext()
    fmt.create_dummy_axis()
    fmt = '%.2e'
    plt.clabel(contours, inline=True, fontsize=8, fmt=fmt)

    # create colour bar that aligns height with axes
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="3%", pad=0.05)
    ax.figure.colorbar(im, cax=cax)
    # add label to each cell
    it = np.nditer(contour_var, flags=['multi_index'])
    if label:
        add_label_to_plot(ax, contour_var.reshape(env.shape))
    return ax


def heatmap_imshow(env, heatmap_var, ax):
    return ax.imshow(heatmap_var.reshape(env.shape), cmap=plt.get_cmap(DEFAULT_CMAP),
                     extent=[0, env.shape[1], env.shape[0], 0], origin='upper')


def plot_quiver_heatmap_var(env, heatmap_var, pi, height=7, filename=None, policy_label="*", beta=None, zoom=False,
                            auto=False, clim=None, negative=False, label=False, title=False):
    ax = plot_quiver(env, pi, policy_label=policy_label, filename=filename, beta=beta, zoom=zoom, auto=auto)

    if negative:
        im = heatmap_imshow(env, -heatmap_var, ax)
    else:
        im = heatmap_imshow(env, heatmap_var, ax)
    add_rectangle_to_grid_for_goals(ax, env)
    # create colour bar that aligns height with axes
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="3%", pad=0.05)
    ax.figure.colorbar(im, cax=cax)
    if clim is not None:
        im.set_clim(clim)
    ax.set_title("")

    if title:
        ax.set_title(title, x=0.1, y=1.08, **title_font)

    if label:
        add_label_to_plot(ax, heatmap_var.reshape(env.shape), 0.35)

    if filename:
        plt.savefig(filename,
                    bbox_inches='tight',
                    pad_inches=0)

    return ax


def plot_quiver_heats_val(env, heatmap_var, values, pi, height=7, filename=None, policy_label="*", beta=None, zoom=False,
                            auto=False, clim=None, negative=False, label=False, title=False):
    ax = plot_quiver(env, pi, policy_label=policy_label, filename=filename, beta=beta, zoom=zoom, auto=auto)

    if negative:
        im = heatmap_imshow(env, -heatmap_var, ax)
    else:
        im = heatmap_imshow(env, heatmap_var, ax)
    add_rectangle_to_grid_for_goals(ax, env)
    # create colour bar that aligns height with axes
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="3%", pad=0.05)
    ax.figure.colorbar(im, cax=cax)
    if clim is not None:
        im.set_clim(clim)
    ax.set_title("")

    if title:
        ax.set_title(title, x=0.1, y=1.08, **title_font)

    if label:
        add_label_to_plot(ax, values.reshape(env.shape), 0.25)

    if filename:
        plt.savefig(filename,
                    bbox_inches='tight',
                    pad_inches=0)

    return ax


def update_quiver_heatmap(ax, im, vector, cb):
    # update heatmap and colorbar
    im.set_data(vector)
    im.set_clim(np.amin(vector), np.amax(vector))
    # draw new colorbar in existing cax
    # cb = plt.colorbar(im, ax=ax, cax=cax)
    cb.locator = ticker.AutoLocator()
    cb.update_ticks()
    # update quiver plot


def plot_trade_off_curve(env, betas, E_Information, E_Performance, algorithm_name, dist_name, filename=None, ylim=None):
    points = len(betas)
    plt.plot(np.asarray(E_Information), np.asarray(E_Performance), "k.-", linewidth=0.5, markersize=3)
    fig = plt.gcf()
    plt.xlabel(r'Expected Information')
    plt.ylabel(r'Expected Performance')
    ax = plt.gca()
    # ax.set_xlim([xmin, xmax])
    if ylim:
        plt.ylim(ylim)
    ymin = min(E_Performance)
    # ax.set_ylim([1.05*ymin, max(0.5*max(E_Performance), 0.5*ymin)])

    plt.text(0.5, 0.4,
             "{}\nshape: {}\ngoal state: {} \n{} State Distribution"
             .format(algorithm_name, env.shape, env.goals, dist_name),
             transform=ax.transAxes, verticalalignment='top', horizontalalignment='left')
    interval = points//5
    for count, beta in zip(range(points), betas[0::interval]):
        plt.annotate(r'$\beta$={}'.format(format_number(beta)),
                     xy=(E_Information[count*interval] * 0.95, E_Performance[count*interval] * 0.95))
    if filename is not None:
        fig.savefig(filename, bbox_inches='tight', pad_inches=0.1)
    plt.show()


def visualise_iteration(env, generator, policy, title=None, label=False):
    """
    Step through iterative calculation using the generator to visualise progress and detect any oscillations.
    Generator passes the next iteration of the a matrix (nS, nA) so the policy is used to calculate the expected
    heatmap_var and plot a heatmap of the matrix over the grid.
    :param env: environment
    :param generator: either to return next iteration of information-to-go-full or free energy
    :param policy: p(a|s) shape (nS, nA)
    :return:  Matrix tests for conversion and returns the final converged heatmap_var of the matrix.
    TODO for this to work we need to deduce a valid linthresh from the first iteration of the matrix.
    """
    fig, ax = plt.subplots(figsize=(15, 15))
    ax.set_title(title, **title_font)
    steps = next(generator)
    update_policy = False
    if 'pi' in steps.keys():
        update_policy = True
        policy = steps['pi']

    M = get_expected_vector(policy, steps['M']).reshape(env.shape)
    im = ax.imshow(M, cmap=plt.get_cmap(DEFAULT_CMAP))
    add_rectangle_to_grid_for_goals(ax, env)
    add_rectangle_to_grid_for_walls(ax, env)
    vmax = np.amax(ma.masked_invalid(M))
    vmin = np.amin(ma.masked_invalid(M))
    im.set_clim(vmin, vmax)
    labels = []
    if label:
        labels = add_label_to_plot(ax, M)
    # create colour bar that aligns height with axes
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="3%", pad=0.05)

    cb = plt.colorbar(im, ax=ax, cax=cax)
    plt.ion()
    plt.show(block=False)
    plt.draw()
    try:
        while True:
            steps = next(generator)
            if update_policy:
                policy = steps['pi']
            M = get_expected_vector(policy, steps['M']).reshape(env.shape)
            vmax = np.amax(ma.masked_invalid(M))
            vmin = np.amin(ma.masked_invalid(M))
            im.set_data(M)
            im.set_clim(vmin, vmax)
            # im.set_array(M)
            # draw new colorbar in existing cax
            # cb = plt.colorbar(im, ax=ax, cax=cax)
            cb.locator = ticker.AutoLocator()
            cb.update_ticks()
            if label:
                for label, new_value in zip(labels, M.flatten()):
                    label.set_text(format_number(np.round(new_value, 2)))
            plt.draw()
            plt.pause(0.01)
    except GeneratorExit:
        # generator raises GeneratorExit when iteration converges
        plt.close()
        pass
    return steps


def select_state_colours(shape):
    colour_options = {(3, 3): [0, 1, 0,
                               1, 2, 1,
                               0, 1, 0],
                      (4, 4): [0, 1, 1, 0,
                               1, 2, 2, 1,
                               1, 2, 2, 1,
                               0, 1, 1, 0],

                      (5, 5): [6, 7, 7,  7, 6,
                               7, 8, 9,  8, 7,
                               7, 9, 10, 9, 7,
                               7, 8, 9,  8, 7,
                               6, 7, 7,  7, 6],

                      (7, 7): [ 4,  5,  5,  5,  5,  5,  4,
                                5,  6,  7,  7,  7,  6,  5,
                                5,  7,  8,  9,  8,  7,  5,
                                5,  7,  9, 10,  9,  7,  5,
                                5,  7,  8,  9,  8,  7,  5,
                                5,  6,  7,  7,  7,  6,  5,
                                4,  5,  5,  5,  5,  5,  4],

                      (7, 5): [0, 1, 1, 1, 0,
                               1, 2, 3, 2, 1,
                               1, 3, 4, 3, 1,
                               1, 4, 6, 4, 1,
                               1, 3, 4, 3, 1,
                               1, 2, 3, 2, 1,
                               0, 1, 1, 1, 0],
                      (11, 11): [ 0,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0,
                                  1,  2,  3,  3,  3,  3,  3,  3,  3,  2,  1,
                                  1,  3,  4,  5,  5,  5,  5,  5,  4,  3,  1,
                                  1,  3,  5,  6,  7,  7,  7,  6,  5,  3,  1,
                                  1,  3,  5,  7,  8,  9,  8,  7,  5,  3,  1,
                                  1,  3,  5,  7,  9, 10,  9,  7,  5,  3,  1,
                                  1,  3,  5,  7,  8,  9,  8,  7,  5,  3,  1,
                                  1,  3,  5,  6,  7,  7,  7,  6,  5,  3,  1,
                                  1,  3,  4,  5,  5,  5,  5,  5,  4,  3,  1,
                                  1,  2,  3,  3,  3,  3,  3,  3,  3,  2,  1,
                                  0,  1,  1,  1,  1,  1,  1,  1,  1,  1,  0]
                      }
    return colour_options[shape]


graph_cmap = mpl.colors.ListedColormap([
    mcolors.cnames['firebrick'],      # 0 outer corners
    mcolors.cnames['lightcoral'],     # 1 outer edge
    mcolors.cnames['darkorange'],     # 2 2nd corner
    mcolors.cnames['orange'],         # 3 edge
    mcolors.cnames['darkgoldenrod'],  # 4 3th corner
    mcolors.cnames['gold'],           # 5 edge
    mcolors.cnames['steelblue'],      # 6 4th corner
    mcolors.cnames['cornflowerblue'], # 7 edge
    mcolors.cnames['darkviolet'],     # 8 5th corner
    mcolors.cnames['blueviolet'],     # 9 edge
    mcolors.cnames['crimson']         # 10 centre
])