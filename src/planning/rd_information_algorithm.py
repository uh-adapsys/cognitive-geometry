from planning.policy import Policy
from planning.state_distribution import StateDistribution
import utility.information_theory as it
import numpy as np
import gmpy2 as mp
import inspect
from abc import ABC, abstractmethod


class RDInformationAlgorithm(ABC):
    def __init__(self, env, state_dist, theta=1e-3, **kwargs):
        """
        Information Algorithm base class.
        Uses convention of Open AI Gym pertaining to the transition model to capture successor states
        and the reward function.

        p0_s: state prior - uniform distribution, stored in StateDistribution object

        Args:
            env: OpenAI env. env.P represents the transition probabilities of the environment.
                env.P[s][a] is a list of transition tuples (prob, next_state, reward, done).
                env.nS_legal is a number of states in the environment.
                env.nA is a number of actions in the environment.
            theta: tolerance for testing convergence,  stop recursive evaluation once utility or policy
                change (maximum over all states) is less than epsilon - see utility.information_theory
        """
        self.env = env
        self.theta = theta
        self.state_dist = state_dist

    def policy_blahut_step(self, p_a_k, exponential):
        """
        Rate Distortion optimisation for communicating signal X -> Y
        using Lagrange Multiplier beta and distortion matrix D
        multi precision floating arithmetic may be required owing to exponential
            p_a_G_s_next = p_a_k * exp[-exponential].normalised
            p_a_k = the marginal distribution of the policy @ prior state distribution
                np.ma.masked_invalid(policy).filled(0).T @ self.p0_s
            p_a_G_s represents the policy, i.e. the conditional probability P(a|s)
            invalid states (walls) set to np.nan

        useful information about mpfr:
        https://stackoverflow.com/questions/26539163/elementwise-operations-in-mpmath-slow-compared-to-numpy-and-its-solution?lq=1
        https://www.programcreek.com/python/example/98526/gmpy2.mpz

        Params:
            p_a_k:  marginal action distribution based on current policy estimate and prior state distribution
            exponential:  Free energy for Information-to-go beta*D for Rate Distortion (nS, nA)

        Returns:
            next estimate of policy p_a_G_s shape (nS, nA) and Z, the partition function
        """
        p_a_k = np.tile(p_a_k, (exponential.shape[0], 1)).flatten()
        try:
            E = exponential.flatten()
            exp_term = np.array([mp.exp2(x) for x in E])
            result = np.array([mp.mul(x, y) for (x, y) in zip(exp_term, p_a_k)]).reshape(exponential.shape)
            Z = np.array([mp.fsum(row) for row in result])
            policy = np.array([mp.div(single, total) for (single, total) in
                               zip(result.flatten(), np.repeat(Z,  self.env.nA))]).reshape(exponential.shape)
            policy = policy.astype(float)
            policy[np.where(policy < 1 / 10 ** 12)] = 0
            # policy[np.argwhere(policy == 0)] += 1e-303
            return policy, Z

        except FloatingPointError as err:
            # use multiple precision
            print("Floating point error - even though using mpfr {}".format(inspect.currentframe()))
            print(mp.get_context())
            print("Error: {} ".format(err))
            exit(7)

    @abstractmethod
    def get_opt_policy_Q(self, **kwargs):
        raise NotImplementedError

    def get_zero_state_action_matrix(self):
        """
        return a 2 dimensional array of shape number of total states by number of actions (nS_grid, nA)
        where the invalid wall states are set to np.nan and the remaining valid states to zero
        :return: numpy.ndarray of type float64 shape (nS_grid, nA)
        """
        matrix = np.zeros((self.env.nS, self.env.nA))  # action heatmap_var function for utility
        return matrix

    def get_zero_state_vector(self):
        """
        return a 2 dimensional array of shape number of total states by number of actions (nS_grid, nA)
        where the invalid wall states are set to np.nan and the remaining valid states to zero
        :return: numpy.ndarray of type float64 shape (nS_grid, nA)
        """
        vector = np.zeros(self.env.nS)
        return vector


    @staticmethod
    def get_marginal_action_probability(policy, p_s):
        """
        Calculate the marginal - Pr{A = a}.
        Fill the policy np.nan values with zero for @ matrix operator

        :param policy: numpy.ndarray of type float64 with shape (nS, nA)
        :return: p0_a: prior action probability distribution
        """
        return policy.T @ p_s




