import numpy as np
from utility.information_theory import conditional_kullback_leibler_divergence

# TODO rewrite as a class policy convergence is independent of the algorithm


def test_convergence(U, U_old, policy, policy_old, p0_s, eps_U=1e-3, eps_pi=1e-7):
    return test_policy(eps_pi, policy, policy_old, p0_s) and test_utility(eps_U, U, U_old)


def test_utility(eps_U, U, U_old):
    """
    rel_tol is a relative tolerance, it is multiplied by the greater of the magnitudes of the two arguments; as the
    values get larger, so does the allowed difference between them while still considering them equal. abs_tol is an
    absolute tolerance that is applied as-is in all cases. If the difference is less than either of those tolerances,
    the values are considered equal.
    :param eps_U: relative tolerance
    :param U:
    :param U_old:
    :return: True if U and U_old are close to within eps_U
    """
    #np.isclose(U, U_old, eps_U)
    return np.isclose(U, U_old, atol=eps_U, equal_nan=True).all()


def test_policy(eps_pi, policy, policy_old, p0_s):
    # to cope with np.nan fill with zero use np.nan_to_num
    Dkl = conditional_kullback_leibler_divergence(np.nan_to_num(policy), np.nan_to_num(policy_old), p0_s)
    return (Dkl - eps_pi) <= 0

