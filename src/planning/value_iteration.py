import numpy as np
import pandas as pd
from pprint import PrettyPrinter

from env.grid_room import GridRoom
import utility.display as display
from planning.policy import Policy


class ValueIteration:
    def __init__(self, env, gamma=1, theta=1e-5):
        self.env = env
        self.gamma = gamma
        self.theta = theta
        self.opt_policy = None
        self.opt_value = None

    def one_step_lookahead(self, state, value):
        A = np.zeros(self.env.nA)
        for a in range(self.env.nA):
            for prob, next_state, reward, done in self.env.D[state][a]:
                A[a] += prob * (reward + self.gamma * value[next_state])
        return A

    def step_value_iteration(self):
        V = np.zeros(self.env.nS)
        while True:
            delta = 0
            for s in self.env.D.keys():
                # Do a one-step lookahead to find the best action
                V_s_A = self.one_step_lookahead(s, V)
                best_action_value = np.max(V_s_A)
                # Calculate delta across all states seen so far
                delta = max(delta, np.abs(best_action_value - V[s]))
                # Update the value function. Ref: Sutton book eq. 4.10.
                V[s] = best_action_value
            yield V
            # Check for convergence
            if delta < self.theta:
                raise GeneratorExit
                break
        return V

    def get_optimal_value(self):
        generator = self.step_value_iteration()
        try:
            while True:
                V = next(generator)
        except GeneratorExit:
            pass
        self.opt_value = pd.DataFrame(V.reshape(self.env.shape))
        return V

    def get_optimal_policy(self, V_opt):
        # Create a deterministic planning policy using the optimal value function
        policy = np.zeros([self.env.nS, self.env.nA])
        for s in self.env.D.keys():
            # One step lookahead to find the best action for this state
            A = self.one_step_lookahead(s, V_opt)
            # best_action = np.argmax(A)
            best_actions = np.argwhere(A == np.amax(A))
            for action in best_actions:
                # update the planning to reflect the best_action(s)
                policy[s, action] = 1.0 / len(best_actions)
        self.opt_policy = pd.DataFrame(policy)
        return policy

    def get_opt_policy_utility(self):
        V = self.get_optimal_value()
        pi = self.get_optimal_policy(V)
        return pi, V

    def __str__(self):
        pp = PrettyPrinter(2)
        v_heading = "\noptimal state value function:\n"
        pi_heading = "\noptimal policy:\n"
        env_heading = "\n\ngrid world:\n"
        return pi_heading + str(self.opt_policy) + env_heading + str(self.env) + v_heading + pp.pformat(self.opt_value)


def main():
    options = {'shape': (10, 10), 'walls': [], 'goals': [99],
               'mines': [34, 35, 36, 43, 46, 42, 53, 54]}
    env = GridRoom(options)
    vi = ValueIteration(env, theta=1e-7)
    pi_opt, V_opt = vi.get_opt_policy_utility()
    print(vi)
    display.plot_quiver_heatmap_var(env, V_opt, pi_opt)
    import matplotlib.pyplot as plt
    plt.show()


if __name__ == '__main__':
    main()
