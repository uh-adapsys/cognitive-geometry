from planning.rd_information_algorithm import RDInformationAlgorithm
from planning.policy import Policy
from env.grid_room import GridRoom
from utility import information_theory as it
from planning.convergence import test_utility

import numpy as np


class RelevantInformation(RDInformationAlgorithm):
    def __init__(self, env, state_dist, theta=1e-5):
        self.name = 'Relevant Information'
        super(RelevantInformation, self).__init__(env, state_dist, theta)

    def get_opt_policy_utility(self, beta):
        policy, Q = self.get_opt_policy_Q(beta)
        V = it.get_expected_vector(policy, Q)
        return policy, V

    def get_opt_policy_Q(self, beta):
        """
           Relevant Information Algorithm to calculate the policy that minimises
           the relevant_information given beta and gamma.  This algorithm uses a blahut
           arimoto step to perform the Lagrangian optimisation.

           Args:
               theta: epsilon to test for convergence
               env: OpenAI env. env.P represents the transition probabilities of the environment.
                   env.P[s][a] is a list of transition tuples (prob, next_state, reward, terminal).
                   env.nS is a number of states in the environment.
                   env.nA is a number of actions in the environment.
                   env.p_s - typically the initial state distribution - prior p(s)
               beta: Lagrangian multiplier - trade off between utility and relevant information

           Returns:
               A tuple (policy, U) policy which minimises relevant_information while achieving performance
           """

        policy = Policy.get_uniform_policy(self.env)
        self.state_dist.set_ps(policy)
        Q = self.get_zero_state_action_matrix()
        # multiple precision context
        # ctx = mp.get_context()
        # ctx.precision = 200
        # ctx.trap_invalid = True
        while True:
            policy_old = np.copy(policy)
            Q_old = np.copy(Q)
            if self.state_dist.update:
                p_s, Dkl = self.state_dist.update_ps(policy, threshold=0.2)
            p_a_k = self.get_marginal_action_probability(policy, self.state_dist.p_s)  # calculate the marginal - Pr{A = a}
            policy, _ = self.policy_blahut_step(p_a_k, beta * Q_old)
            Q = self.get_action_value_utility(policy_old, Q_old)
            if test_utility(self.theta, Q, Q_old):
                p_s, _ = self.state_dist.update_ps(policy, threshold=1e-5)
                break
        return policy, Q

    def get_action_value_utility(self, policy, V_old, gamma=1.0):
        """
        Return expected with one step look ahead for each state, action pair
        q_policy(s,a) = E[Rt+1 + gamma v_policy(st+1)|St=s, At=a]

        Returns:
            Q[s][a] rate-heatmap_var matrix for state, action pairs
        """
        Q = self.get_zero_state_action_matrix()
        for s in self.env.D.keys():
            for a in range(self.env.nA):
                for p_ss_G_a_s, ss, reward, _ in self.env.D[s][a]:
                    # calculate rewards from all successor states of s
                    Q[s][a] += reward
                    for sa in range(self.env.nA):  # successor action - one step look ahead
                        Q[s][a] += gamma * policy[ss][sa] * V_old[ss][sa]
                    Q[s][a] *= p_ss_G_a_s
        return Q

    def get_mutual_information_by_state(self, policy):
        return it.get_mutual_information_by_state(policy, self.state_dist.p_s)

    def get_trade_off_data(self, betas):
        E_Information = []
        E_Performance = []
        for beta in betas:
            pi_opt, Q_opt = self.get_opt_policy_Q(beta=beta)
            V_opt = it.get_expected_vector(pi_opt, Q_opt)
            E_Performance.append(it.get_expected_scalar(self.state_dist.p_s, V_opt))
            E_Information.append(it.get_expected_scalar(self.state_dist.p_s, self.get_mutual_information_by_state(pi_opt)))
            print("beta: {:5g} E[RI]{:.5g} E[V]{:.5g}".format(beta, E_Information[-1], E_Performance[-1]))
        print('\n')
        return E_Information, E_Performance


def main():
    from planning.state_distribution import LiveStateDistribution
    shape = (4, 4)
    points = 20
    goals = [0]
    beta = 6.30957
    theta = 1e-3
    options = {'shape': shape, 'goals': goals, 'manhattan': True, 'determinism': 1}
    env = GridRoom(options)
    ri = RelevantInformation(env, LiveStateDistribution(env), theta)
    pi_opt, Q_opt = ri.get_opt_policy_Q(beta=beta)
    from utility import display
    V_opt = it.get_expected_vector(pi_opt, Q_opt)
    display.plot_quiver_heatmap_var(env, V_opt, pi_opt)
    import matplotlib.pyplot as plt
    plt.show()
    print('beta: {:8.8g}'.format(beta))
    print('E[U(S,A)]: {:8.8g}'.format(it.get_expected_scalar(ri.state_dist.p_s, V_opt)))
    ri_by_state = ri.get_mutual_information_by_state(pi_opt)
    print('I(S;A): {:8.8g}'.format(it.get_expected_scalar(ri.state_dist.p_s, ri_by_state)))
    betas = np.logspace(-3, 2, num=points)
    E_Information, E_Performance = ri.get_trade_off_data(betas)
    display.plot_trade_off_curve(env, betas, E_Information, E_Performance, ri.name, ri.state_dist.name)

    env.twist_world(0.5)
    betas = np.logspace(-3, 2, num=points)
    E_Information, E_Performance = ri.get_trade_off_data(betas)
    display.plot_trade_off_curve(env, betas, E_Information, E_Performance, ri.name, ri.state_dist.name)

if __name__ == '__main__':
    main()
