from pprint import PrettyPrinter

import numpy as np
import pandas as pd

from env.grid_room import GridRoom
from planning.convergence import test_utility
import utility.display as display
from utility.exceptions import PolicyNotKnownError


class Policy:
    """
    gridFour does not keep place holders for illegal states. the index of the state is the state id
    Policy uses a pandas dataframe to store the policy, action and states automatically labelled with id.
    Policy contains a reference to the environment it relates to.
    Policy needs to be explicitly set, there is no default.
    """
    def __init__(self, env, incoming_policy=None):
        self.env = env
        self.pi = None
        if incoming_policy is not None:
            self.set_policy(incoming_policy)

    def __str__(self, indent=4):
        pp = PrettyPrinter(indent)
        heading = "grid shape: {} \npolicy: [nS x nA]: states in rows, actions in columns\n".format(self.env.shape)
        return heading + pp.pformat(self.pi)

    def set_policy(self, policy):
        """
        Store the policy passed as a ndarray.  Could be used in conjunction with the random and uniform policy methods.
        e.g. self.set_policy(self.get_uniform_policy())
        :param policy: ndarray of shape (nS_grid, nA)
        """
        self.pi = pd.DataFrame(policy)

    def get_ndarray(self):
        return self.pi.values

    @staticmethod
    def get_random_policy(env):
        """
        Calculate and return a random policy array, states in rows, actions in columns.
        Sum across row must total 1 to be a valid probability distribution.
        :return: array of shape (nS, nA) where each element policy[s][a] is Pr(A=a | S=s)
        """
        random_policy = np.random.random_sample((env.nS, env.nA))
        # normalise policy
        random_policy = random_policy / np.sum(random_policy, axis=1)[:, None]
        return random_policy

    @staticmethod
    def get_uniform_policy(env):
        """
        Calculates and returns a uniform policy array, states in rows, actions in columns.
        Sum across row must total 1 to be a valid probability distribution.
        :return: array of shape (nS, nA) where each element policy[s][a] is Pr(A=a | S=s)
        """
        uniform_policy = np.ones((env.nS, env.nA)) / env.nA
        return uniform_policy

    def get_V_Q(self, epsilon=1e-5):
        return Policy.evaluate_policy(self.env, self.pi.values, epsilon)

    @staticmethod
    def evaluate_policy(env, policy, theta=1e-5):
        """
        Evaluate a policy given an environment and a full description of the environment's dynamics.
        Iterative policy evaluation algorithm taken from Sutton & Barto section 4.1
        Args:
            policy: [S, A] shaped matrix representing the policy - if not specified uses the stored policy
            env: contains dynamics - transition probabilities for (s, a) pairs
            theta: default 1e-5, stop evaluating once the max change in value for the states is less than epsilon
        Returns:
            tuple (V, Q)
                V: vector of length env.nS representing the state value function.
                Q: matrix of shape (nS, nA) representing the state-action value function.
        """
        V = np.zeros(env.nS)
        Q = np.zeros((env.nS, env.nA))
        while True:
            V_old = np.copy(V)
            Q = np.zeros((env.nS, env.nA))
            V = np.zeros(env.nS)
            for s in env.D.keys():
                for a in range(env.nA):
                    if policy[s][a] > 1e-8:
                        for p_ss_G_a_s, ss, reward, _ in env.D[s][a]:
                            v = p_ss_G_a_s * (reward + V_old[ss])
                            V[s] += policy[s][a] * v
                            Q[s][a] += v

            if test_utility(theta, V, V_old):
                break
        # return np.array(V), np.array(Q)
        return V, Q

    def plot_quiver(self, height=7, policy_label="*", filename=None, beta=None, zoom=False, auto=False):
        if self.pi is None:
            raise PolicyNotKnownError
        return display.plot_quiver(self.env, self.pi.values, policy_label=policy_label, filename=filename, beta=beta, zoom=zoom, auto=auto)


def main():
    env = GridRoom()
    policy = Policy(env, Policy.get_uniform_policy(env))
    value, _ = Policy.evaluate_policy(env, Policy.get_uniform_policy(env), 1e-5)
    print(policy)
    policy.plot_quiver(policy_label='u')
    print(policy.get_V_Q())
    policy.set_policy(policy.get_random_policy(env))

    import matplotlib.pyplot as plt
    plt.show()
    print(policy)
    policy.plot_quiver(policy_label='r')
    plt.show()


if __name__ == '__main__':
    main()
