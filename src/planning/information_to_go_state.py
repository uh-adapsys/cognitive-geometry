from env.grid_room import GridRoom
from planning.policy import Policy

from utility import display as di
from utility import information_theory as it
import matplotlib.pyplot as plt
from planning.state_distribution import LiveStateDistribution, StationaryStateDistribution, UniformStateDistribution
from planning.convergence import test_convergence, test_utility
from planning.rd_information_algorithm import RDInformationAlgorithm


import numpy as np
import gmpy2 as mp


class InformationToGoState(RDInformationAlgorithm):
    def __init__(self, env, state_dist, theta=1e-5):
        self.name = 'State Information-to-go '
        self.beta = None
        super(InformationToGoState, self).__init__(env, state_dist, theta)

    def get_opt_policy_Q(self, beta):
        generator = self.get_opt_policy_Z_free_vector(beta)
        steps = {}
        try:
            while True:
                steps = next(generator)
        except GeneratorExit:
            pass
        return steps['policy'], steps['matrix'].astype(float)

    def get_Q_F(self, F):
        Q = self.env.T.values * (self.env.R.values - np.tile(F, (self.env.nS * self.env.nA, 1)))
        return np.sum(Q, axis=1).reshape((self.env.nS, self.env.nA))

    def get_F_Z(self, F, policy, p_s, beta):
        p_a = self.get_marginal_action_probability(policy, p_s)
        Q_F = self.get_Q_F(F)
        policy, Z = self.policy_blahut_step(p_a, beta * Q_F)
        # F = np.multiply(-1/beta, np.log2(Z.astype(float)))
        # higher precision needed for higher beta
        F = np.array([mp.mul(-1 / beta, mp.log(z) / mp.log(2)) for z in Z])
        return policy, Z, F, p_a

    def get_opt_policy_Z_free_vector(self, beta):
        self.beta = beta
        F = self.get_zero_state_vector()
        policy = self.state_dist.policy
        p_s = self.state_dist.set_ps(policy)
        count = 0
        while True:

            F_old = np.copy(F)
            policy_old = np.copy(policy)
            policy, Z, F, p_a = self.get_F_Z(F, policy, p_s, beta)

            """
            tried two options to speed up simulation:
            * update p_s only every 50 iterations
            if not count % 50:
                print('iteration: {} - p(a): {}'.format(count, p_a))
                p_s = self.state_dist.set_ps(policy)
            * nested test for convergence with updated p_s - occassional singular matrix
            
             if test_convergence(F.astype(float), F_old.astype(float), policy, policy_old, p_s, self.theta, self.theta):
                print('iteration: {} - p(a): {}  --------- updated p_s '.format(count, p_a))
                p_s = self.state_dist.set_ps(policy)
                F_old = np.copy(F)
                Z_old = np.copy(Z)
                policy_old = np.copy(policy)
                policy, Z, F, p_a = self.get_F_Z(F, policy, p_s, beta)
            
            if not count % 50:
                # print('iteration: {} - p(a): {}'.format(count, p_a))
                # p_s = self.state_dist.set_ps(policy)
            # yield {'matrix': F, 'policy': policy}
            """

            if test_convergence(F.astype(float), F_old.astype(float), policy, policy_old, p_s, self.theta, self.theta):
                # print('converged at iteration: {} p(a): {}'.format(count, p_a))
                p_s = self.state_dist.set_ps(policy)
                policy, Z, F, p_a = self.get_F_Z(F, policy, p_s, beta)
                # print('converged at iteration: {} p(a): {}'.format(count, p_a))
                break
            count += 1
            if count > 10000:
                print('**********************  forced exit ********* ')
                break

        # print('policy: {}'.format(policy))
        # print('Z: {}'.format(Z.astype(float)))
        # print('F: {}'.format(F.astype(float)))
        return policy, Z.astype(float), F.astype(float)


    def get_information_to_go_given_policy(self, policy):
        I = self.get_zero_state_vector()
        p_s = self.state_dist.set_ps(policy)
        p_a = self.get_marginal_action_probability(policy, p_s)
        dec_complexity = it.log2_ignore_zero(it.div_ignore_zero(policy, np.tile(p_a, (self.env.nS, 1))))
        count = 0
        while True:
            I_old = np.copy(I)
            I = self.get_zero_state_vector()
            for s in np.arange(self.env.nS):
                if s in self.env.goals:
                    I[s] = 0
                else:
                    for a in np.arange(self.env.nA):
                        if policy[s][a] > 1e-12:
                            for p_ss_G_a_s, ss, reward, done in self.env.D[s][a]:
                                I[s] += policy[s][a] * p_ss_G_a_s * (dec_complexity[s][a] +  I_old[ss])
            if test_utility(self.theta, I, I_old):
                # print('converged at iteration {}'.format(count))
                break
            count += 1
            if count > 100_000:
                print('forced exit')
                break
        # print('I: {}'.format(np.array2string(I, formatter={'float_kind':lambda x: "%.4g" % x})))
        return I

    def get_trade_off_data(self, betas):
        E_Information = []
        E_Performance = []
        for beta in betas:
            # print('beta: {}'.format(beta))
            policy, _, free_energy = self.get_opt_policy_Z_free_vector(beta=beta)
            information = self.get_information_to_go_given_policy(policy)
            V, _ = Policy(self.env, policy).get_V_Q()
            E_Performance.append(it.get_expected_scalar(self.env.isd, V))
            E_Information.append(it.get_expected_scalar(self.env.isd, information))
            print("beta: {:3.8f}  |  performance: {:.8f}  |  information: {:.8f}".format(beta, E_Performance[-1], E_Information[-1]))
        return E_Information, E_Performance


def main():
    shape = (11, 11)
    theta = 1e-5
    goals = [int(np.product(shape)/2)]
    goals = [60]
    beta = 0.1
    options = {'shape': shape, 'goals': goals, 'manhattan': True, 'determinism': True}
    env = GridRoom(options)
    state_dist = LiveStateDistribution(env)
    print(env)

    ig = InformationToGoState(env, state_dist, theta)
    policy, Z, F = ig.get_opt_policy_Z_free_vector(beta)

    pi = Policy(env, policy)
    print(pi)
    di.plot_quiver_heatmap_var(env, F.astype(float), policy)

    plt.show()
    I = ig.get_information_to_go_given_policy(policy)
    di.plot_quiver_heatmap_var(env, I.astype(float), policy)

    plt.show()
    p_s = ig.state_dist.set_ps(policy)
    di.plot_quiver_heatmap_var(env, p_s.astype(float), policy)
    plt.show()

    print(pi)
    print(np.sum(policy, axis=1))


if __name__ == '__main__':
    main()