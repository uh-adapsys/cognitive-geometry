import utility.information_theory as it

import numpy as np


def get_stationary_distribution_eigen_decomposition(env, pi):
    # stationary distribution... expect them all to be the goal without teleporting
    p_s_s = StateDistribution.build_transition_matrix(env, pi)
    p_s_s = StateDistribution.add_teleporting_to_transition_matrix(env, p_s_s)
    lam, x = np.linalg.eig(p_s_s.T)
    idx = np.argmin(np.abs(lam - 1))
    p_s = np.real(x[:, idx])
    p_s = p_s / p_s.sum()
    return p_s


def get_stationary_distribution_qr_decomposition(env, pi):
    pass


def get_live_distribution_fundamental_matrix(env, pi):
    # Grinstead and Snell 2006
    # Using Fundamental Matrix
    p_s_s = StateDistribution.build_transition_matrix(env, pi)
    Q = np.delete(np.delete(p_s_s, env.goals, axis=0), env.goals, axis=1)
    t_states = Q.shape[0]
    try:
        N = np.linalg.inv(np.eye(t_states) - Q)
    except np.linalg.LinAlgError as err:
        print('******************** Error inverting matrix: ', err)
        pass

    t = N @ np.ones(t_states)
    t = np.tile(t, (t_states, 1)).T
    p_si_g_sj = np.divide(N, t)
    p0_si = np.ones(t_states).T / t_states
    p_s = p_si_g_sj.T @ p0_si
    p_s[np.abs(p_s) < 1e-6] = 0
    """
    # identified negative probabilities close to zero
    # removed by the line above
    with np.errstate(invalid='ignore'):
        if np.any(p_s < 0):
            print('*** zero probability: {}'.format(p_s[p_s < 0])) """
    p_s = np.insert(p_s, env.goals, 0, axis=0)
    return p_s


class StateDistribution:
    """
    initial state distribution defaults to uniform distribution across all states
    get_state_distribution returns the initial state distribution
    """
    def __init__(self, env):
        self.env = env
        self.policy = np.ones((env.nS, env.nA)) / env.nA
        self.isd = np.ones(env.nS)/env.nS
        self.p_s = self.set_ps(self.policy)
        self.name = "Default"
        self.update = False

    def reset(self, env):
        self.policy = np.ones((self.env.nS, self.env.nA)) / self.env.nA
        self.set_ps(self.policy)
        self.env = env

    # def get_ps(self):
        # return self.p_s

    def set_ps(self, policy):
        # must be overridden for other distributions
        self.p_s = np.ones(self.env.nS)/self.env.nS
        self.policy = policy
        return self.p_s

    def update_ps(self, policy, threshold):
        Dkl = it.conditional_kullback_leibler_divergence(policy, self.policy, self.p_s)
        if Dkl > threshold:
            self.policy = np.copy(policy)
            self.p_s = self.set_ps(policy)
        return self.p_s, Dkl

    @staticmethod
    def build_transition_matrix(env, pi):
        p_s_s = np.zeros((env.nS, env.nS))
        for s_i in range(env.nS):
            for s_j in range(env.nS):
                for a in range(env.nA):
                    p_s_s[s_i][s_j] += pi[s_i, a] * env.T[s_j][s_i * env.nA + a]
        return p_s_s

    @staticmethod
    def add_teleporting_to_transition_matrix(env, p_s_s):
        # now to add teleporting...
        for goal in env.goals:
            p_s_s[goal][:] = 1 / (env.nS - 1)
            p_s_s[goal][goal] = 0
        return p_s_s


class UniformStateDistribution(StateDistribution):
    def __init__(self, env):
        super(UniformStateDistribution, self).__init__(env)
        self.name = "Uniform"


class LiveStateDistribution(StateDistribution):
    def __init__(self, env):
        super(LiveStateDistribution, self).__init__(env)
        self.name = "Live"
        self.update = True

    def set_ps(self, policy):
        self.p_s = get_live_distribution_fundamental_matrix(self.env, policy)
        self.ps = it.set_zeros(self.p_s)
        self.policy = policy
        return self.p_s


class StationaryStateDistribution(StateDistribution):
    def __init__(self, env):
        super(StationaryStateDistribution, self).__init__(env)
        self.name = "Stationary"
        self.update = True

    def set_ps(self, policy):
        self.p_s = get_stationary_distribution_eigen_decomposition(self.env, policy)
        self.ps = it.set_zeros(self.p_s)
        self.policy = policy
        return self.p_s



