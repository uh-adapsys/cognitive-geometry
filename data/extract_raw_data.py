import os
from sys import argv
import pickle


def load_data_from_file(filename):
    try:
        infile = open(filename, 'rb')
        data_dict = pickle.load(infile)
        print(filename, "loaded", flush=True)
        return data_dict
    except FileNotFoundError:
        print("File {} not found, exiting.".format(filename), flush=True)
        exit(1)
    except Exception as inst:
        print("exception: ", type(inst))
        print("args:", inst.args)
        print(inst, ", exiting.")
        exit(1)


if __name__ == '__main__':
    dirname = None
    """ simply pass dirname as arg """
    if len(argv) > 1:
        dirname = argv[1]
    else:
        print("no directory specified")
        exit(1)

    try:
        os.chdir(dirname)
    except OSError:
        print('directory not found:', dirname)

    regex = "(?<=data-).+?(?=.pickle)"
    for infile in os.listdir(os.getcwd()):
        if os.path.splitext(infile)[1] == ".pickle":
            try:
                data_dict_in = load_data_from_file(infile)
                data_dict_out = {}
                data_dict_out['frees'] = data_dict_in['frees']
                data_dict_out['infos'] = data_dict_in['infos']
                data_dict_out['policies'] = data_dict_in['policies']
                data_dict_out['values'] = data_dict_in['values']
                env = data_dict_in['env']
                data_dict_out['G'] = env.G
                data_dict_out['nS'] = env.nS
                data_dict_out['beta'] = data_dict_in['beta']
                data_dict_out['shape'] = env.shape
                data_dict_out['D'] = env.D
                head, tail = os.path.split(infile)
                outfile = os.path.splitext(infile)[0] + "-raw" + ".pickle"
                pickle.dump(data_dict_out, open(outfile, "wb"))
                print('{} saved'.format(outfile), flush=True)
            except:
                print('exception: ', infile, flush=True)
                pass


