# A space of goals: the cognitive geometry of informationally bounded agents

## Code and data repository for Decision Geometry

This repository contains the code and data files relating to the paper entitled "A space of goals: the cognitive geometry of informationally bounded agents" accepted for publication by Royal Society Open Science.

The source code and data are contained in their respective directories.  Several jupyter notebooks are available in the 'notebooks' directory, which demonstrate how to setup an environment and calculate the Decision Information for that environment.  In addition, all code requred to generate the figures in the paper is available as notebooks in the 'geometry-paper' directory.  

This repository has been archived within the Zenodo repository: DOI: 10.5281/zenodo.7273869 https://zenodo.org/deposit/7273869#.