from setuptools import setup, find_packages

"""
gmpy2 has several dependencies
apt-get install libgmp-dev
apt-get install libmpfr-dev
apt-get install libmpc-dev
"""

setup(
    name='gridSim',
    version='0.6',
    packages=find_packages(where='src'),
    package_dir={'': 'src'}, install_requires=['numpy', 'scipy', 'matplotlib', 'pytest', 'networkx', 'pandas', 'gmpy2', 'scikit-learn']
)
